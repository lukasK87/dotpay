<?php


class dotpay extends PaymentModule
{


    use \Components\Traits\LoggerTrait;


    /**
     * Module version. Make sure to increase version any time you add some
     * new functions in this class!
     * @var string
     */
    protected $version = '1.2019-05-17';

    /**
     * Module name, visible in admin portal.
     * @var string
     */
    protected $modname = 'dotPay';

    /**
     * Module description, visible in admin portal
     * @var string
     */
    protected $description = 'dotPay Online Checkout module';

    protected $supportedCurrencies = ['PLN', 'EUR', 'USD', 'GBP', 'JPY', 'CZK', 'SEK', 'UAH', 'RON', 'BGN', 'CHF', 'HRK', 'HUF', 'RUB'];

    protected $configuration = [

        'Login' => [
            'value' => '',
            'type' => 'input'
        ],
        'Password' => [
            'value' => '',
            'type' => 'input'
        ],
        'dotPay ID' => [
            'value' => '',
            'type' => 'input'
        ],
        'dotPay PIN' => [
            'value' => '',
            'type' => 'input'
        ],

        'Test mode' => [
            'value' => '',
            'description' => 'Tick if your dotPay environment is using Sandbox mode',
            'type' => 'check'
        ],


    ];


    public function __construct()
    {

        parent::__construct();

    }


    /**
     * HostBill will run this function first time module is activated.
     * Use it to create some database tables
     */
    public function install()
    {

    }

    /**
     * HostBill will run this function when admin choose to "Uninstall" module.
     * Use it to truncate/remove tables.
     */
    public function uninstall()
    {

    }

    /**
     * Return HTML code that should be displayed in clientarea for client to pay (ie. in invoice details)
     * @return string
     */




    public function drawForm(){



        return $this->GenerateChkDotpayRedirection();

    }

    public function GenerateChkDotpayRedirection(){

       $ParametersArray = $this->getParametersArray();
       $ChkValue = $this->generateCHK($ParametersArray);



        if ($this->configuration['Test mode']['value']) {
            $EnvironmentAddress = 'https://ssl.dotpay.pl/test_payment/';
        }
        else{
            $EnvironmentAddress = 'https://ssl.dotpay.pl/t2/';
        }

        $RedirectionCode = '<form action="'.$EnvironmentAddress.'" method="POST" id="dotpay_redirection_form" accept-charset="UTF-8">'.PHP_EOL;

        foreach ($ParametersArray as $key => $value) {
                $RedirectionCode .= "\t".'<input name="'.$key.'" value="'.$value.'" type="hidden"/>'.PHP_EOL;
        }


        $RedirectionCode .= "\t".'<input name="chk" value="'.$ChkValue.'" type="hidden"/>'.PHP_EOL;
        $RedirectionCode .= '</form>'.PHP_EOL.'<button class="btn btn-primary" id="dotpay_redirection_button" type="submit" form="dotpay_redirection_form" value="Submit">Confirm and Pay</button>'.PHP_EOL;

            return $RedirectionCode;

    }


    public function getParametersArray(){

        $values = [];

        $values['api_version'] = "dev";
        $values['id'] = $this->configuration['dotPay ID']['value'];
        $values['amount'] = $this->amount;
        $values['currency'] = $this->currency_code;
        $values['description'] = 'Invoice no. '.$this->invoice_id;
        $values['url'] = rtrim(HBConfig::getConfig('InstallURL'), '/') .'?cmd=clientarea&action=invoices';
        $values['urlc'] =  $this->callback_url;
        $values['buttontext'] = 'Go back';
        $values['customer'] = $this->generateCustomerBase64();
        $values['type'] = 0;
        $values['control'] = hash('sha256', $this->client['id'].'_'.$this->invoice_id.'_'.$this->amount.'_'.$this->configuration['dotPay PIN']['value']);
		
		return $values;

    }

    public function generateCustomerBase64(){
        $customer = [
            "payer" => [
                "first_name" => $this->client['firstname'],
                "last_name" => $this->client['lastname'],
                "email" => $this->client['email'],
            ] ,
            "order" => [
                "delivery_address" => [
                    "city" => $this->client['city'],
                    "street" => $this->client['address1'],
                    "building_number" => $this->client['address2'],
                    "postcode" => $this->client['postcode'],
                ]
            ]
        ];

        return base64_encode(json_encode($customer));
    }

    public function generateCHK($ParametersArray){

         $chk =   $this->configuration['dotPay PIN']['value'].
            (isset($ParametersArray['api_version']) ? $ParametersArray['api_version'] : null).
            (isset($ParametersArray['lang']) ? $ParametersArray['lang'] : null).
            (isset($ParametersArray['id']) ? $ParametersArray['id'] : null).
            (isset($ParametersArray['pid']) ? $ParametersArray['pid'] : null).
            (isset($ParametersArray['amount']) ? $ParametersArray['amount'] : null).
            (isset($ParametersArray['currency']) ? $ParametersArray['currency'] : null).
            (isset($ParametersArray['description']) ? $ParametersArray['description'] : null).
            (isset($ParametersArray['control']) ? $ParametersArray['control'] : null).
            (isset($ParametersArray['channel']) ? $ParametersArray['channel'] : null).
            (isset($ParametersArray['credit_card_brand']) ? $ParametersArray['credit_card_brand'] : null).
            (isset($ParametersArray['ch_lock']) ? $ParametersArray['ch_lock'] : null).
            (isset($ParametersArray['channel_groups']) ? $ParametersArray['channel_groups'] : null).
            (isset($ParametersArray['onlinetransfer']) ? $ParametersArray['onlinetransfer'] : null).
            (isset($ParametersArray['url']) ? $ParametersArray['url'] : null).
            (isset($ParametersArray['type']) ? $ParametersArray['type'] : null).
            (isset($ParametersArray['buttontext']) ? $ParametersArray['buttontext'] : null).
            (isset($ParametersArray['urlc']) ? $ParametersArray['urlc'] : null).
            (isset($ParametersArray['firstname']) ? $ParametersArray['firstname'] : null).
            (isset($ParametersArray['lastname']) ? $ParametersArray['lastname'] : null).
            (isset($ParametersArray['email']) ? $ParametersArray['email'] : null).
            (isset($ParametersArray['street']) ? $ParametersArray['street'] : null).
            (isset($ParametersArray['street_n1']) ? $ParametersArray['street_n1'] : null).
            (isset($ParametersArray['street_n2']) ? $ParametersArray['street_n2'] : null).
            (isset($ParametersArray['state']) ? $ParametersArray['state'] : null).
            (isset($ParametersArray['addr3']) ? $ParametersArray['addr3'] : null).
            (isset($ParametersArray['city']) ? $ParametersArray['city'] : null).
            (isset($ParametersArray['postcode']) ? $ParametersArray['postcode'] : null).
            (isset($ParametersArray['phone']) ? $ParametersArray['phone'] : null).
            (isset($ParametersArray['country']) ? $ParametersArray['country'] : null).
            (isset($ParametersArray['code']) ? $ParametersArray['code'] : null).
            (isset($ParametersArray['p_info']) ? $ParametersArray['p_info'] : null).
            (isset($ParametersArray['p_email']) ? $ParametersArray['p_email'] : null).
            (isset($ParametersArray['n_email']) ? $ParametersArray['n_email'] : null).
            (isset($ParametersArray['expiration_date']) ? $ParametersArray['expiration_date'] : null).
            (isset($ParametersArray['deladdr']) ? $ParametersArray['deladdr'] : null).
            (isset($ParametersArray['recipient_account_number']) ? $ParametersArray['recipient_account_number'] : null).
            (isset($ParametersArray['recipient_company']) ? $ParametersArray['recipient_company'] : null).
            (isset($ParametersArray['recipient_first_name']) ? $ParametersArray['recipient_first_name'] : null).
            (isset($ParametersArray['recipient_last_name']) ? $ParametersArray['recipient_last_name'] : null).
            (isset($ParametersArray['recipient_address_street']) ? $ParametersArray['recipient_address_street'] : null).
            (isset($ParametersArray['recipient_address_building']) ? $ParametersArray['recipient_address_building'] : null).
            (isset($ParametersArray['recipient_address_apartment']) ? $ParametersArray['recipient_address_apartment'] : null).
            (isset($ParametersArray['recipient_address_postcode']) ? $ParametersArray['recipient_address_postcode'] : null).
            (isset($ParametersArray['recipient_address_city']) ? $ParametersArray['recipient_address_city'] : null).
            (isset($ParametersArray['application']) ? $ParametersArray['application'] : null).
            (isset($ParametersArray['application_version']) ? $ParametersArray['application_version'] : null).
            (isset($ParametersArray['warranty']) ? $ParametersArray['warranty'] : null).
            (isset($ParametersArray['bylaw']) ? $ParametersArray['bylaw'] : null).
            (isset($ParametersArray['personal_data']) ? $ParametersArray['personal_data'] : null).
            (isset($ParametersArray['credit_card_number']) ? $ParametersArray['credit_card_number'] : null).
            (isset($ParametersArray['credit_card_expiration_date_year']) ? $ParametersArray['credit_card_expiration_date_year'] : null).
            (isset($ParametersArray['credit_card_expiration_date_month']) ? $ParametersArray['credit_card_expiration_date_month'] : null).
            (isset($ParametersArray['credit_card_security_code']) ? $ParametersArray['credit_card_security_code'] : null).
            (isset($ParametersArray['credit_card_store']) ? $ParametersArray['credit_card_store'] : null).
            (isset($ParametersArray['credit_card_store_security_code']) ? $ParametersArray['credit_card_store_security_code'] : null).
            (isset($ParametersArray['credit_card_customer_id']) ? $ParametersArray['credit_card_customer_id'] : null).
            (isset($ParametersArray['credit_card_id']) ? $ParametersArray['credit_card_id'] : null).
            (isset($ParametersArray['blik_code']) ? $ParametersArray['blik_code'] : null).
            (isset($ParametersArray['credit_card_registration']) ? $ParametersArray['credit_card_registration'] : null).
            (isset($ParametersArray['surcharge_amount']) ? $ParametersArray['surcharge_amount'] : null).
            (isset($ParametersArray['surcharge']) ? $ParametersArray['surcharge'] : null).
            (isset($ParametersArray['surcharge']) ? $ParametersArray['surcharge'] : null).
            (isset($ParametersArray['ignore_last_payment_channel']) ? $ParametersArray['ignore_last_payment_channel'] : null).
            (isset($ParametersArray['vco_call_id']) ? $ParametersArray['vco_call_id'] : null).
            (isset($ParametersArray['vco_update_order_info']) ? $ParametersArray['vco_update_order_info'] : null).
            (isset($ParametersArray['vco_subtotal']) ? $ParametersArray['vco_subtotal'] : null).
            (isset($ParametersArray['vco_shipping_handling']) ? $ParametersArray['vco_shipping_handling'] : null).
            (isset($ParametersArray['vco_tax']) ? $ParametersArray['vco_tax'] : null).
            (isset($ParametersArray['vco_discount']) ? $ParametersArray['vco_discount'] : null).
            (isset($ParametersArray['vco_gift_wrap']) ? $ParametersArray['vco_gift_wrap'] : null).
            (isset($ParametersArray['vco_misc']) ? $ParametersArray['vco_misc'] : null).
            (isset($ParametersArray['vco_promo_code']) ? $ParametersArray['vco_promo_code'] : null).
            (isset($ParametersArray['credit_card_security_code_required']) ? $ParametersArray['credit_card_security_code_required'] : null).
            (isset($ParametersArray['credit_card_operation_type']) ? $ParametersArray['credit_card_operation_type'] : null).
            (isset($ParametersArray['credit_card_avs']) ? $ParametersArray['credit_card_avs'] : null).
            (isset($ParametersArray['credit_card_threeds']) ? $ParametersArray['credit_card_threeds'] : null).
            (isset($ParametersArray['customer']) ? $ParametersArray['customer'] : null).
            (isset($ParametersArray['gp_token']) ? $ParametersArray['gp_token'] : null);


        return hash('sha256', $chk);

    }

    public function callback(){

        $result = self::PAYMENT_FAILURE;

        $sign =
            $this->configuration['dotPay PIN']['value'].
            $_POST['id'].
            $_POST['operation_number'].
            $_POST['operation_type'].
            $_POST['operation_status'].
            $_POST['operation_amount'].
            $_POST['operation_currency'].
            $_POST['operation_withdrawal_amount'].
            $_POST['operation_commission_amount'].
            $_POST['is_completed'].
            $_POST['operation_original_amount'].
            $_POST['operation_original_currency'].
            $_POST['operation_datetime'].
            $_POST['operation_related_number'].
            $_POST['control'].
            $_POST['description'].
            $_POST['email'].
            $_POST['p_info'].
            $_POST['p_email'].
            $_POST['credit_card_issuer_identification_number'].
            $_POST['credit_card_masked_number'].
            $_POST['credit_card_expiration_year'].
            $_POST['credit_card_expiration_month'].
            $_POST['credit_card_brand_codename'].
            $_POST['credit_card_brand_code'].
            $_POST['credit_card_unique_identifier'].
            $_POST['credit_card_id'].
            $_POST['channel'].
            $_POST['channel_country'].
            $_POST['geoip_country'];

        $signature=hash('sha256', $sign);

        if($_POST['signature']!=$signature){
            $log = 'Wrong signature: ' . json_encode($_POST);
        } elseif(!isset($_POST['id'])|| !isset($_POST['operation_number'])) {
              $log = 'Unknown response: ' . json_encode($_POST);
          } else{

                  if($_POST['operation_status']!='completed'){
                        $log = 'Bad operation status: '.json_encode($_POST);
                  } else{
                        $description = $_POST['description'];

                        $invoice_id = (int)str_replace('Invoice no. ', '', $description);

                        $invoice = Invoice::createInvoice($invoice_id);

                        if($invoice->getInvoiceId() != $invoice_id){

                            $log = 'Invalid Invoice no.: '.json_encode($_POST);
                        } else{

                            $hash = hash('sha256', $invoice->getClientId().'_'.$invoice_id.'_'.$_POST['operation_original_amount'].'_'.$this->configuration['dotPay PIN']['value']);

                            if($hash!=$_POST['control']){
                                $log = 'Invalid hash: '.json_encode($_POST);
                            } else{

                                $result = self::PAYMENT_SUCCESS;

                                $transaction_id = $_POST['operation_number'];

                                if (!$this->_transactionExists($transaction_id)) {
                                    $this->addTransaction([
                                        'invoice_id' => $invoice_id,
                                        'client_id' => $invoice->getClientId(),
                                        'in' => $_POST['operation_original_amount'],
                                        'transaction_id' => $transaction_id,
                                        'description' => 'dotPay Payment ' . $invoice_id.' Total paid: '.$_POST['operation_amount'],
                                        'fee' => 0
                                    ]);
                                }
                                $log['transaction'] = $transaction_id;

                                echo "OK";
                            }

                        }

                 }

            }

        $this->logActivity([
            'output' => $log,
            'result' => $result
        ]);

    }

    /**
     * Handle payment refund
     *
     * @param $_transaction
     */
    public function refund($_transaction, $amount) {

        $control = hash('sha256', $_transaction.'_'.$amount.'_'.$this->configuration['dotPay PIN']['value']);

        $query = $this->db->prepare("SELECT `description`, `in` FROM hb_transactions WHERE `trans_id` = :trans_id AND `module` = :module_id");
        $query->execute([
            'trans_id' => $_transaction,
            'module_id' => $this->getModuleId()
        ]);
        $ref = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();

         $amount_in_PLN = round((float)substr($ref['description'], strpos($ref['description'], ':') + 1) * ($amount/$ref['in']) , 2);



        $fields = [
            'amount' => $amount_in_PLN,
            'description' => 'Refund for transaction '.$_transaction,
            'control' => $control,
        ];

		if($this->configuration['Test mode']['value']){
			$url = 'https://ssl.dotpay.pl/test_seller/api/v1/payments/'.$_transaction.'/refund/';
		}
		else{
			$url = 'https://ssl.dotpay.pl/s2/login/api/v1/payments/'.$_transaction.'/refund/';
		}			

		$data=json_encode($fields, 320);
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url );
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, False);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt ($ch, CURLOPT_CAINFO, "ca-bundle.crt");
        curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt ($ch, CURLOPT_USERPWD, $this->configuration['Login']['value'].':'.$this->configuration['Password']['value']);
        curl_setopt ($ch, CURLOPT_POST, 1);
        curl_setopt ($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt ($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: '.strlen($data)]);
        $response = curl_exec($ch);
        $response = json_decode($response, true);
        $err = curl_error($ch);

        curl_close($ch);


        if($err){
            $this->logActivity(array(
                'output' => ['cURL error: ' => (array)$err],
                'result' => self::PAYMENT_FAILURE
            ));

            return false;

        }
        else{
            if ($response['detail']=='ok') {
                $result = $this->addRefund(array(
                    'target_transaction_number' => $_transaction,
                    'transaction_id' => $control,
                    'amount' => $amount
                ));

                $this->logActivity(array(
                    'output' => (array)$response,
                    'result' => $result ? self::PAYMENT_SUCCESS : self::PAYMENT_FAILURE
                ));
                return $result;

            }
            else{
                $this->logActivity(array(
                    'output' => ['error' => json_encode($response)],
                    'result' => self::PAYMENT_FAILURE
                ));

                return false;
            }
        }


    }

}